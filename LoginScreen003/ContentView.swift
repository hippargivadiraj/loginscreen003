//
//  ContentView.swift
//  LoginScreen003
//
//  Created by Leadconsultant on 11/3/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
     @State var phoneNumber = " "
     @State var countryCode = "001"
    
    
    var body: some View {
        GeometryReader{ geometry in
            
            //FirstScreen
        ZStack{
            Rectangle().foregroundColor(.gray).opacity(0.4) //Background Rectangle
            VStack{
                
                ZStack{
                    Rectangle().foregroundColor(.red).cornerRadius(100, antialiased: true, corners: [.bottomLeft]).frame(height: geometry.size.height/3)
                    VStack(spacing:20){
                        Image(systemName: "arrow.up.arrow.down.square").resizable().frame(width: 50, height: 50, alignment: .center).foregroundColor(.white)
                        Text("WONDERCARD").font(.title).foregroundColor(.white)
                    }
                }
                
                ZStack{
                    Rectangle().foregroundColor(.clear)
                    
                    VStack(spacing:10){
                        Text("login").font(.title).foregroundColor(.red)
                        HStack(spacing: 2){
                            TextField("Country ", text: self.$countryCode).textFieldStyle(RoundedBorderTextFieldStyle()).frame(width: 50,  alignment: .center)
                            TextField(" ", text: self.$phoneNumber).textFieldStyle(RoundedBorderTextFieldStyle()).frame(width: 250,  alignment: .center)
                        }.padding()
                        Button("Get Verification Code"){
                            print("Get Verification Pressed")
                        }.padding().background(Color.red).cornerRadius(10, antialiased: true, corners: [.allCorners]).foregroundColor(.white)
                        
                        
                    }.offset(y:  ( -geometry.size.height * 0.18 ))
                }
                
                
            }
            
            }
        }.edgesIgnoringSafeArea(.all).accentColor(.red)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
