//
//  cornerRadius.swift
//  LoginScreen003
//
//  Created by Leadconsultant on 11/12/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

//This view extension helps to make round corner for views for chosen corner
extension View {
    func cornerRadius(_ radius: CGFloat, antialiased: Bool = true, corners: UIRectCorner) -> some View {
        clipShape(
            RoundedCorner(radius: radius, style: antialiased ? .continuous : .circular, corners: corners)
        )
    }
}
