//
//  RoundedCorner.swift
//  LoginScreen003
//
//  Created by Leadconsultant on 11/12/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

//This is a Shape that clips shapes bound to rounded corners
struct RoundedCorner: Shape {
    
    var radius: CGFloat = .infinity
    var style: RoundedCornerStyle = .continuous
    var corners: UIRectCorner = .allCorners
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
